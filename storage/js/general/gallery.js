$(document).ready(function(){

    $(".deletePic").click(function (e) {
        var $this = $(this);
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "./delete-pic",
            data: {id: $(this).data('id'), name: $(this).data("name")},
            success: function(data){

                if(data['result'] === true){
                    $this.parent().parent('.jqEachImg').remove();
                }
            }
        });
        $(document).ajaxStop(function(){
            window.location.reload();
        });
    });

});





