

    $(document).ready(function() {
        $(".tab-content").niceScroll({
            cursorcolor: "red",
            railoffset:{ bottom:0 },
            cursorfixedheight:35,
        });
        $(".gallery-item").niceScroll({
            cursorcolor: "red",
            railoffset:{ bottom:0 },
            cursorfixedheight:35,
        });
        $(".news-height").niceScroll({
            cursorcolor: "red",
            railoffset:{ bottom:0 },
            cursorfixedheight:35,
        });
        $(".call-us-panell").niceScroll({
            cursorcolor: "red",
            railoffset:{ bottom:0 },
            cursorfixedheight:35,
        });
        $(".about").niceScroll({
            cursorcolor: "red",
            railoffset:{ bottom:0 },
            cursorfixedheight:35,
        });

        $('.carousel').carousel({
            interval: 2000
        });
        $('.owl-carousel').owlCarousel({
            rtl:true,
            loop:true,
            margin:10,
            nav:true,
            navText : ["<i class='fa fa-chevron-right'></i>","<i class='fa fa-chevron-left'></i>"],
            responsive:{
                0:{
                    items:2
                },
                600:{
                    items:3
                },
                800:{
                    items:5
                },
                1000:{
                    items:7
                }
            }

        });


    });

    function myMap() {
        var mapOptions1 = {
            center: new google.maps.LatLng(51.508742,-0.120850),
            zoom:9,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var mapOptions2 = {
            center: new google.maps.LatLng(51.508742,-0.120850),
            zoom:9,
            mapTypeId: google.maps.MapTypeId.SATELLITE
        };
        var mapOptions3 = {
            center: new google.maps.LatLng(51.508742,-0.120850),
            zoom:9,
            mapTypeId: google.maps.MapTypeId.HYBRID
        };
        var mapOptions4 = {
            center: new google.maps.LatLng(51.508742,-0.120850),
            zoom:9,
            mapTypeId: google.maps.MapTypeId.TERRAIN
        };
        var map1 = new google.maps.Map(document.getElementById("googleMap1"),mapOptions1);
        var map2 = new google.maps.Map(document.getElementById("googleMap2"),mapOptions2);
        var map3 = new google.maps.Map(document.getElementById("googleMap3"),mapOptions3);
        var map4 = new google.maps.Map(document.getElementById("googleMap4"),mapOptions4);
    }

