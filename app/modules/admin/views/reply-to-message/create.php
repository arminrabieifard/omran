<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ReplyToMessage */

$this->title = 'پاسخ به پیام کاربران';
$this->params['breadcrumbs'][] = ['label' => 'پاسخ به پیام کاربران', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reply-to-message-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'messageModel' => $messageModel
    ]) ?>

</div>
