<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ادمین';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <div class="box-body">

        <div class="user-index">

            <p>
                <?= Html::a('افزودن ادمین', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <?php Pjax::begin(); ?>    <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        //['class' => 'yii\grid\SerialColumn'],

                        'id',
                        'full_name',
                        'user_name',
                        //'password',
                        //'auth_key',
                        // 'password_reset_token',
                        // 'email:email',
                        'active',
                        // 'create_at',
                        [
                            'attribute' => 'create_at',
                            'format' => 'raw',
                            'content' => function($data){
                                return \app\components\General::persianDate($data->create_at);
                            }
                        ],

                        ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
