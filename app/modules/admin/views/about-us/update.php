<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AboutUs */

$this->title = 'ویرایش درباره ما ' ;
$this->params['breadcrumbs'][] = ['label' => 'درباره ما', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'ویرایش';
?>
<div class="about-us-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
