<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AboutUs */

$this->title = 'افزودن درباره ما';
$this->params['breadcrumbs'][] = ['label' => 'درباره ما', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="about-us-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
