<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AboutUs */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <div class="box-body">

        <div class="about-us-form">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

            <div class="form-group">
                <label for="">متن</label>

                <?= \dosamigos\tinymce\TinyMce::widget([
                    'name' => 'AboutUs[description]',
                    'value' => $model->description,
                    'language' => 'fa',
                    'clientOptions' => \app\components\General::getTinyMceConfig(),
                ]);
                ?>
            </div>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'افزودن' : 'ویرایش', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
