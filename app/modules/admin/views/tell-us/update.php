<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TellUs */

$this->title = 'ویرایش تماس با ما: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'تماس با ما', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'ویرایش';
?>
<div class="tell-us-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
