<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TellUs */

$this->title = 'افزودن تماس با ما';
$this->params['breadcrumbs'][] = ['label' => 'Tell uses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tell-us-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
