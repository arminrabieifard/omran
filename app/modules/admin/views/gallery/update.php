<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Gallery */
/* @var $imageModel \app\modules\admin\models\ImagesUploader*/

$this->title = 'ویرایش گالری : ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'گالری', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'ویرایش';
?>
<div class="gallery-update">

    <?= $this->render('_form', [
        'model' => $model,
        'imageModel' => $imageModel,

    ]) ?>

</div>
