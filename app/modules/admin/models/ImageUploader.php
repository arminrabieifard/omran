<?php
namespace app\modules\admin\models;


use Yii;
use yii\web\UploadedFile;

class ImageUploader extends \yii\base\Model
{

    public $imageFile;

    public function rules()
    {
        return [

            [['imageFile'], 'file','extensions' => 'jpeg, jpg, png', 'checkExtensionByMimeType' => true],
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'on' => 'create'],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'on' => 'update'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'imageFile' => 'عکس',
        ];
    }

    public function upload($picName, $path, $widthSize = null, $heightSize = null)
    {
        if($this->validate())
        {
            $gd = Yii::$app->gd;
            $gd->load($this->imageFile->tempName);
            $gd->resizeToWidth($widthSize);

            /** create directory if It isn't exist */
            if(!is_dir(Yii::getAlias('@uploads-root') . '/' . $path . '/')) mkdir(Yii::getAlias('@uploads-root') . '/' . $path . '/', 0777, true);

            $gd->save(Yii::getAlias('@uploads-root') . '/' . $path . '/' . $picName);
            return true;

        }
        else
        {
            return $this->errors;
        }
    }
    public function uploadSlider($picName, $path, $widthSize = null, $heightSize = null)
    {
        if($this->validate())
        {
            $gd = Yii::$app->gd;
            $gd->load($this->imageFile->tempName);
            $gd->resizeToHeight($widthSize);

            /** create directory if It isn't exist */
            if(!is_dir(Yii::getAlias('@uploads-root') .'/' . $path . '/')) mkdir(Yii::getAlias('@uploads-root') . '/' . $path . '/', 0777, true);

            $gd->save(Yii::getAlias('@uploads-root'). '/'  . $path . '/' . $picName);
            return true;

        }
        else
        {
            return $this->errors;
        }
    }
    public function updateUpload($picName, $path, $oldFileName, $widthSize = null, $heightSize = null)
    {
        if($this->validate())
        {
            @unlink(Yii::getAlias('@uploads-root') . '/' . $path . '/' . $oldFileName);

            $gd = Yii::$app->gd;
            $gd->load($this->imageFile->tempName);
            $gd->resizeToWidth($widthSize);

            /** create directory if It isn't exist */
            if(!is_dir(Yii::getAlias('@uploads-root') . '/' . $path . '/')) mkdir(Yii::getAlias('@uploads-root') . '/' . $path . '/', 0777, true);

            $gd->save(Yii::getAlias('@uploads-root') . '/' . $path . '/' . $picName);
            return true;

        }
        else
        {
            return $this->errors;
        }
    }
}