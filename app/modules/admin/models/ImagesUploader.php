<?php
namespace app\modules\admin\models;


use Yii;
use yii\web\UploadedFile;

class ImagesUploader extends \yii\base\Model
{

    /**
     * @var $imageFiles UploadedFile[]
     */
    public $imageFiles;
    public function rules()
    {
        return [
            [['imageFiles'], 'file','extensions' => 'jpeg, jpg, png', 'checkExtensionByMimeType' => true ,'skipOnEmpty' => false, 'maxFiles' => 10, 'on' => 'create'],
            [['imageFiles'], 'file','extensions' => 'jpeg, jpg, png', 'checkExtensionByMimeType' => true ,'skipOnEmpty' => true, 'maxFiles' => 10, 'on' => 'update'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'imageFiles' => 'عکس',
        ];
    }


    public function uploadMultiImage($path)
    {
        $picNameArr = $errorsPic = [];
        $gd = Yii::$app->gd;


        foreach ($this->imageFiles as $item)
        {
            $gd->load($item->tempName);
            $gd->resizeToWidth(700);
            $picName =  Yii::$app->security->generateRandomString(5) . time() . '.' . $item->extension;
            $picNameArr[] = $picName;

            if(!is_dir(Yii::getAlias('@uploads-root') . '/' . $path . '/')) mkdir(Yii::getAlias('@uploads-root') . '/' . $path . '/', 0777, true);

            $gd->save(Yii::getAlias('@uploads-root') . '/' . $path . '/' . $picName);
        }


        return $picNameArr;
    }
}