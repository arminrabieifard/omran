<?php

namespace app\modules\admin\controllers;

use app\components\General;
use app\modules\admin\models\ImageUploader;
use Yii;
use app\models\Slider;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * SliderController implements the CRUD actions for Slider model.
 */
class SliderController extends CustomController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Slider models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Slider::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Slider model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Slider model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Slider();
        $imageModel = new ImageUploader();
        $imageModel->scenario = 'create';

        if ($model->load(Yii::$app->request->post())) {

            $imageModel->imageFile = UploadedFile::getInstance($imageModel,'imageFile');
            $picName = $imageModel->imageFile->baseName . '_' . Yii::$app->security->generateRandomString(5) . time() . '.' . $imageModel->imageFile->extension;

            if($result = $imageModel->uploadSlider($picName, 'slider', '550'))
            {

                $model->file_name = $picName;
                if($model->save())
                {
                    Yii::$app->session->setFlash('alert', ['success', 'عملیات مورد نظر با موفقیت انجام  شد.']);
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
            else
            {
                Yii::$app->session->setFlash('alert', ['danger', General::showSummaryError($result)]);
                return $this->render('create', [
                    'model' => $model,
                    'imageModel' => $imageModel,
                ]);
            }

        } else {
            return $this->render('create', [
                'model' => $model,
                'imageModel' => $imageModel,
            ]);
        }
    }

    /**
     * Updates an existing Slider model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $imageModel = new ImageUploader();
        $imageModel->scenario = 'update';

        if(($imageModel->imageFile = UploadedFile::getInstance($imageModel,'imageFile')) !== null)
        {
            $picName = $imageModel->imageFile->baseName . '_' . Yii::$app->security->generateRandomString(5) . time() . '.' . $imageModel->imageFile->extension;
            if($result = $imageModel->updateUpload($picName, 'slider', $model->file_name, 700))
            {
                $model->file_name = $picName;
            }
            else
            {
                Yii::$app->session->setFlash('alert', ['success', 'عملیات مورد نظر با موفقیت انجام  شد.']);
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('alert', ['success', 'عملیات مورد نظر با موفقیت انجام  شد.']);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'imageModel' => $imageModel,
            ]);
        }
    }

    /**
     * Deletes an existing Slider model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model =  $this->findModel($id);
        $model->delete();
        @unlink(Yii::getAlias('@uploads-url') . '/slider/'.  $model->file_name);
        return $this->redirect(['index']);


    }

    /**
     * Finds the Slider model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Slider the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Slider::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
