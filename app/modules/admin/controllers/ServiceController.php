<?php

namespace app\modules\admin\controllers;

use Yii;

use app\models\Service;
use app\components\General;
use app\models\ServiceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use app\modules\admin\models\ImageUploader;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;



/**
 * ServiceController implements the CRUD actions for Service model.
 */
class ServiceController extends CustomController
{
    /**
     * Lists all Service models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ServiceSearch();
        $sort = ['id' => SORT_DESC];
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $sort);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Service model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Service model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Service();
        $imageModel = new ImageUploader();
        $imageModel->scenario = 'create';

        if ($model->load(Yii::$app->request->post())) {

            $imageModel->imageFile = UploadedFile::getInstance($imageModel,'imageFile');
            $picName = $imageModel->imageFile->baseName . '_' . Yii::$app->security->generateRandomString(5) . time() . '.' . $imageModel->imageFile->extension;


            if($result = $imageModel->upload($picName, 'service', 700))
            {


                $model->file_name = $picName;
                $model->create_at = time();

                if($model->save())
                {

                    Yii::$app->session->setFlash('alert', ['success', 'عملیات مورد نظر با موفقیت انجام  شد.']);
                    return $this->redirect(['site-map/index','section' => 'service', 'page' => 'view' ,'id' => $model->id]);
                }
            }
            else
            {
                Yii::$app->session->setFlash('alert', ['danger', General::showSummaryError($result)]);
                return $this->render('create', [
                    'model' => $model,
                    'imageModel' => $imageModel,
                ]);
            }

        } else {
            return $this->render('create', [
                'model' => $model,
                'imageModel' => $imageModel,
            ]);
        }
    }

    /**
     * Updates an existing Service model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $imageModel = new ImageUploader();
        $imageModel->scenario = 'update';

        if(($imageModel->imageFile = UploadedFile::getInstance($imageModel,'imageFile')) !== null)
        {
            $picName = $imageModel->imageFile->baseName . '_' . Yii::$app->security->generateRandomString(5) . time() . '.' . $imageModel->imageFile->extension;
            if($result = $imageModel->updateUpload($picName, 'service', $model->file_name, 700))
            {
                $model->file_name = $picName;
            }
            else
            {
                Yii::$app->session->setFlash('alert', ['success', 'عملیات مورد نظر با موفقیت انجام  شد.']);
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('alert', ['success', 'عملیات مورد نظر با موفقیت انجام  شد.']);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'imageModel' => $imageModel ,
            ]);
        }
    }

    /**
     * Deletes an existing Service model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        @unlink(Yii::getAlias('@storage-root') . '/image/front/service/' . $model->file_name);
        $model->delete();
        Yii::$app->session->setFlash('alert', ['success', 'عملیات مورد نظر با موفقیت انجام  شد.']);
        return $this->redirect(['index']);
    }


    /**
     * Finds the Service model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Service the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Service::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
