<?php
/**
 * Created by PhpStorm.
 * User: lvl
 * Date: 11/17/2017
 * Time: 8:18 AM
 */

namespace app\controllers;

use app\models\News;
use Yii;
use yii\data\Pagination;
use yii\web\Controller;

class NewsController extends Controller
{

    public function actionIndex()
    {
        $model = News::find()->where(['visible' => 1])->orderBy(['id' => SORT_DESC])->all();



        return $this->render('index', [
            'model' => $model,

        ]);
    }
    public function actionView($id)
    {
        $model = News::find()->where(['id' => $id])->one();
        $model->num_view =$model->num_view * 1  + 1  ;
        $model->save();

        return $this->render('view', [
            'model' => $model,
        ]);
    }
}