<?php

namespace app\controllers;

use app\components\General;
use app\models\AboutUs;
use app\models\Gallery;
use app\models\Service;
use app\models\Slider;
use app\models\TellUs;
use app\models\User;
use app\models\UserMessage;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $gallery = Gallery::find()->where(['visible' => 1])->orderBy(['id' => SORT_DESC])->limit(4)->all();
        $service = Service::find()->where(['visible' => 1])->orderBy(['id' => SORT_DESC])->all();

        return $this->render('index', [
            'galleryModel' => $gallery,
            'serviceModel' => $service,

        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {

         $about =AboutUs::find()->one();
         $tell = TellUs::find()->one();

        $model = new UserMessage();
        if ($model->load(Yii::$app->request->post())){
            $model->ip = $_SERVER['REMOTE_ADDR'];

            if($model->save())
            {
                Yii::$app->session->setFlash('alert', ['success', 'عملیات مورد نظر با موفقیت انجام  شد.']);

            }
            else
            {

                return $this->render('about',[
                    'model' => $model,
                    'about' => $about,
                    'tell' => $tell,
                ]);
            }
        }
        return $this->render('about',[
            'model' => $model,
            'about' => $about,
            'tell' => $tell,
            ]);
    }
}
