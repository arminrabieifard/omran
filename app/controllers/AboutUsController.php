<?php

namespace app\controllers;

use app\models\TellUs;
use app\models\UserMessage;
use Yii;
use yii\web\Controller;
use app\models\AboutUs;

class AboutUsController extends Controller
{

    public function actionIndex()
    {
        $about =AboutUs::find()->one();
        $tell = TellUs::find()->one();

        $model = new UserMessage();
        if ($model->load(Yii::$app->request->post())){
            $model->ip = $_SERVER['REMOTE_ADDR'];

            if($model->save())
            {
                Yii::$app->session->setFlash('alert', ['success', 'عملیات مورد نظر با موفقیت انجام  شد.']);

            }
            else
            {

                return $this->render('index',[
                    'model' => $model,
                    'about' => $about,
                    'tell' => $tell,
                ]);
            }
        }
        return $this->render('index',[
            'model' => $model,
            'about' => $about,
            'tell' => $tell,
        ]);
    }

}