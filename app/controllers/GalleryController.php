<?php
/**
 * Created by PhpStorm.
 * User: armin
 * Date: 6/27/2018
 * Time: 5:50 PM
 */

namespace app\controllers;


use app\models\Gallery;
use yii\web\Controller;

class GalleryController extends Controller
{
    public function actionIndex()
    {
        $model = Gallery::find()->orderBy(["id" => SORT_DESC])->all();
        $model2 = Gallery::find()->orderBy(["id" => SORT_ASC])->all();
        return $this->render('index', [
            'model' => $model,
            'model2' => $model2,
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionView($id)
    {
        $model = Gallery::find()->where(['id' => $id])->one();
        $model->num_view =$model->num_view * 1  + 1  ;
        $model->save();

        return $this->render('view', [
            'model' => $model,


        ]);
    }
}