<?php
/**
 * Created by PhpStorm.
 * User: lvl
 * Date: 11/17/2017
 * Time: 8:02 AM
 */

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\TellUs;
use app\models\UserMessage;

class TellUsController extends Controller
{


    /**
     * @return string
     * @var $messageModel UserMessage
     */
    public function actionIndex()
    {
        $model = TellUs::findOne(1);
        $messageModel = new UserMessage();

        if($messageModel->load(Yii::$app->request->post()))
        {
            $messageModel->ip        = Yii::$app->request->userIP;
            $messageModel->status    = 0;
            $messageModel->create_at = time();
            if($messageModel->save())
            {
                Yii::$app->session->setFlash('alert', ['success', 'پیغام شما با موفقیت ارسال شد']);
                return $this->redirect(['index']);
            }
        }

        return $this->render('index',[
            'model' => $model,
            'messageModel' => $messageModel
        ]);
    }
}