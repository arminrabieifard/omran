<?php

use yii\db\Migration;

/**
 * Handles the creation of table `replay_to_message`.
 */
class m171030_194156_create_reply_to_message_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('reply_to_message', [
            'id'              => $this->primaryKey(11)->unsigned(),
            'user_message_id' => $this->integer()->notNull()->unsigned()->comment('مربوط به پیام'),
            'reply'           => $this->text()->comment('متن پاسخ'),
            'visible'     => $this->boolean()->defaultValue(1)->comment('نمایش داده شود؟'),
            'create_at'       => $this->integer()->notNull()->defaultValue(0)->comment('زمان افزودن'),
        ]);

        $this->createIndex(
            'idx-reply-user_message_id',
            'reply_to_message',
            'user_message_id'
        );

        $this->addForeignKey(
            'fk-reply-user_message_id',
            'reply_to_message',
            'user_message_id',
            'user_message',
            'id'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-reply-user_message_id',
            'reply_to_message'
        );

        $this->dropIndex(
            'idx-reply-user_message_id',
            'reply_to_message'
        );

        $this->dropTable('reply_to_message');
    }
}
