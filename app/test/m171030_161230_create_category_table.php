<?php

use yii\db\Migration;

/**
 * Handles the creation of table `category`.
 */
class m171030_161230_create_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('category', [
            'id'        => $this->primaryKey(11)->unsigned(),
            'parent_id' => $this->integer()->unsigned()->comment('زیر شاخه دسته'),
            'title'     => $this->string(50)->comment('عنوان'),
            'file_name' => $this->string(50)->comment('نام فایل'),
            'visible'   => $this->boolean()->defaultValue(1)->comment('نمایش داده شود؟'),
            'create_at' => $this->integer()->notNull()->defaultValue(0)->comment('زمان افزودن'),
        ]);

        $this->createIndex(
            'idx-category-parent_id',
            'category',
            'parent_id'
        );

        $this->addForeignKey(
            'fk-category-parent_id',
            'category',
            'parent_id',
            'category',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }


    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-category-parent_id',
            'category'
        );

        $this->dropIndex(
            'idx-category-parent_id',
            'category'
        );

        $this->dropTable('category');
    }
}
