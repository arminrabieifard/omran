<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tell_us`.
 */
class m171030_185258_create_tell_us_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tell_us', [
            'id'         => $this->primaryKey(11)->unsigned(),
            'telegram'   => $this->string('255')->null()->comment('آدرس تلگرام'),
            'instagram'  => $this->string('255')->null()->comment('آدرس اینستاگرام'),
            'facebook'   => $this->string('255')->null()->comment('آدرس فیسبوک'),
            'email'      => $this->string('255')->null()->comment('آدرس ایمیل'),
            'position_x' => $this->decimal(15,5)->null()->comment('نقطه x نقشه'),
            'position_y' => $this->decimal(15,5)->null()->comment('نقطه y نقشه'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('tell_us');
    }
}
