<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tell_us}}".
 *
 * @property string $id
 * @property string $telegram آدرس تلگرام
 * @property string $instagram آدرس اینستاگرام
 * @property string $facebook آدرس فیسبوک
 * @property string $email آدرس ایمیل
 * @property string $tells تلفن ها
 * @property string $address آدرس
 */
class TellUs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tell_us}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['address'], 'string'],
            [['telegram', 'instagram', 'facebook', 'email'], 'string', 'max' => 255],
            [['tells'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'telegram' => 'آدرس تلگرام',
            'instagram' => 'آدرس اینستاگرام',
            'facebook' => 'آدرس فیسبوک',
            'email' => 'آدرس ایمیل',
            'tells' => 'تلفن ها',
            'address' => 'آدرس',
        ];
    }
}
