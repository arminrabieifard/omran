<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "gallery".
 *
 * @property string $id
 * @property string $category_id
 * @property string $title
 * @property string $description
 * @property string $file_name
 * @property string $images_file
 * @property integer $num_view
 * @property integer $visible
 * @property integer $create_at
 *
 * @property Category $category
 */
class Gallery extends \yii\db\ActiveRecord
{
    public $imageFile;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gallery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'num_view', 'visible', 'create_at'], 'integer'],
            [['description', 'images_file'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['file_name'], 'string', 'max' => 50],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'مربوط به دسته',
            'title' => 'عنوان',
            'description' => 'توضیحات',
            'file_name' => 'نام فایل',
            'images_file' => 'عکسهای ذخیره شده',
            'num_view' => 'تعداد بازدید',
            'visible' => 'نمایش داده شود؟',
            'create_at' => 'زمان افزودن',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public function getCategoryList()
    {
        return ArrayHelper::map(Category::findAll(['visible' => 1]), 'id', 'title');
    }
}
