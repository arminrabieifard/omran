<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot/storage';
    public $baseUrl = '@web/storage';
    public $css = [
        'css/general/bootstrap-rtl.min.css',
        'css/general/font-awesome.min.css',
        'css/general/site.css',

    ];
    public $js = [


        'js/general/bootstrap.min.js',
        'js/general/jquery.nicescroll.min.js',
        'js/general/site.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
