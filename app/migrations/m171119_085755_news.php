<?php

use yii\db\Schema;
use yii\db\Migration;

class m171119_085755_news extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%news}}',
            [
                'id'=> $this->primaryKey(11)->unsigned(),
                'title'=> $this->string(100)->notNull()->comment('عنوان'),
                'description'=> $this->text()->notNull()->comment('توضیحات'),
                'file_name'=> $this->string(50)->null()->defaultValue(null)->comment('نام فایل'),
                'num_view'=> $this->integer(11)->null()->defaultValue(0)->comment('تعداد بازدید'),
                'visible'=> $this->smallInteger(1)->null()->defaultValue(1)->comment('نمایش داده شود؟'),
                'create_at'=> $this->integer(11)->notNull()->defaultValue(0)->comment('زمان افزودن'),
            ],$tableOptions
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{%news}}');
    }
}
