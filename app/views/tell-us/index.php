<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = 'تماس با ما';
?>

<?php $form = ActiveForm::begin(); ?>

    <?= $form->field($messageModel, 'full_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($messageModel, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($messageModel, 'message')->textarea(['rows' => 6]) ?>

    <?= Html::submitButton('ارسال', ['class' => 'btn btn-success']) ?>


<?php ActiveForm::end(); ?>

