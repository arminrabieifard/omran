<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

$this->title = 'لیست خبرها';

$this->registerMetaTag([
    'name' => 'description',
    'content' => '',
]);
?>

<section>
    <div class="col-sm-12 news-item ">
        <div class="col-sm-1 label-item">
            <img  class="img-label" src="<?= Yii::getAlias('@storage-url').'/image/news.png'?>" alt="Los Angeles">
        </div>
        <div class="col-sm-11 news-item-description news-height">
            <?php
            foreach ($model as $value):
            ?>
            <a href="<?=Url::to(["news/view","id" => $value->id])?>">
            <div class="main-news">
                <div class="col-sm-3">
                    <div class="news-image">
                        <img  class="" src="<?= Yii::getAlias('@uploads-url').'/news/'.$value->file_name?>" alt="Los Angeles">
                    </div>
                </div>
                <div class="col-sm-9 ">
                    <div class="news-title">
                        <h2><?= $value->title; ?></h2>
                        <p> <?= $value->description; ?></p>
                    </div>
                </div>
            </div>
            </a>
        <?php endforeach;?>
        </div>
    </div>
</section>