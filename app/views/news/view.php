<?php
/**
 * Created by PhpStorm.
 * User: armin
 * Date: 7/3/2018
 * Time: 4:57 PM
 *
 */
$this->title = $model->title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $model->description,
]);
?>
<section>
    <div class="col-sm-12 news-item ">
        <div class="col-sm-1 label-item">
            <a href="<?= \yii\helpers\Url::to(["news/index"])?>" > <img  class="img-label" src="<?= Yii::getAlias('@storage-url').'/image/news.png'?>" alt="خبرها"></a>
        </div>
        <div class="col-sm-11 news-item-description news-height">

                <div class="main-news">
                    <div class="col-sm-3">

                        <div class="news-image">
                            <img  class="" src="<?= Yii::getAlias('@uploads-url').'/news/'.$model->file_name;?>" alt="<?=$model->title?>">
                        </div>
                    </div>
                    <div class="col-sm-9 ">
                        <div class="news-title">
                            <h2><?=$model->title?></h2>
                            <p><?=$model->description?></p>
                        </div>

                    </div>

                </div>




        </div>
    </div>
</section>
