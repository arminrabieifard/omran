<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="shortcut icon" href="<?= Yii::getAlias('@storage-url').'/image/favicons.ico'?>" type="image/x-icon">
</head>
<body>
<?php
$site ="";
$news ="";
$gallery ="";
$about ="";
$menu = Yii::$app->controller->id;
switch ($menu){
    case 'site':
        $site = "active";
        break;
    case 'news':
        $news = "active";
        break;
    case 'gallery':
        $gallery = "active";
        break;
    case 'about-us':
        $about = "active";
        break;
}
?>
<?php $this->beginBody() ?>

<div class="wrap">
    <div class="">
        <div class="head col-sm-12 col-xs-12  navbar navbar-inverse">
            <nav class="col-sm-12 col-xs-12 " role="navigation">
                <div class="col-xs-12 col-sm-12 ">
                    <div class="navbar-header padd-head">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                </div>
                <div class="collapse  col-sm-10 navbar-collapse col-xs-12" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav ">
                        <li class="<?= $site?>"><a href="<?= Url::to(['/'])?>" >صفحه اصلی </a> <div class="border-left"></div></li>
                        <li class=" <?= $news?>"><a href="<?= Url::to(['/news/'])?>" >خبرها </a> <div class="border-left"></div></li>
                        <li class="<?= $gallery?>"><a href="<?= Url::to(['/gallery/'])?>" >نمونه کارها</a><div class="border-left"></div></li>
                        <li class="<?= $about?>"><a href="<?= Url::to(['/about-us/'])?>"> تماس با ما</a><div class="border-left"></div></li>
                        <li class=""><a href="<?= Url::to(['/about-us/'])?>"> درباره ما  </a></li>
                    </ul>
                </div>
                <div class="contact col-sm-2 col-xs-12 padd-head">
                    <?php
                    $tell = \app\models\TellUs::find()->one();
                    ?>
                    <span><a href="<?= $tell->instagram?>" target="_blank"><img width="30" src="<?= Yii::getAlias('@storage-url').'/image/inista.png' ?>" alt=""/></a></span>
                    <span><a href="<?= $tell->telegram?>" target="_blank"><img width="30" src="<?= Yii::getAlias('@storage-url').'/image/telegram.png' ?>" alt=""/></a></span>

                </div>
            </nav>

        </div>
    </div>

    <div class="container-fluid" style="">
        <div class="slider">

            <div class=" ">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <div class="slider-left">
                        <img src="<?= Yii::getAlias('@storage-url').'/image/logo.png'?>" alt="Los Angeles">
                    </div>
                    <?php
                    $slider = \app\models\Slider::find()->orderBy(["id" => SORT_DESC])->all();
                    ?>
                    <!-- Indicators -->


                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <?php
                        $i = 0;
                        foreach ($slider as $value):
                        ?>
                        <div class="item <?php if($i == 0){ echo "active";} ?> ">
                            <img src="<?= Yii::getAlias('@uploads-url').'/slider/'.$value->file_name;?>" alt="<?= $value->title;?>">
                            <div class="carousel-caption">
                                <h2>ARMAN SAZE IN BULDING</h2>
                                <p><?= $value->title;?> </p>
                            </div>
                        </div>
                        <?php $i++; endforeach; ?>
                    </div>

                    <!-- Left and right controls -->

                </div>
            </div>


        </div>
        <div class="clear-fix"></div>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left copyright">تمامی حقوق این سایت مطعلق است به شرکت عمران پرداز گیلان زمین و هرگونه کپی برداری از آن پیگرد قانونی دارد . </p>

        <p class="pull-right">
            <ul class="footer-menu">
            <li><a href="">صفحه اصلی</a><div class="border-left"></div></li>
            <li><a href="">گالری </a><div class="border-left"></div></li>
            </ul>
        </p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
