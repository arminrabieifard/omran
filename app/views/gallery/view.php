<?php
/**
 * Created by PhpStorm.
 * User: armin
 * Date: 7/3/2018
 * Time: 3:57 PM
 *
/*  @var $model*/
$this->registerCssFile('@web/storage/css/general/fotorama.css');
$this->registerJsFile('@web/storage/js/general/fotorama.js', ['depends' => \yii\web\JqueryAsset::className()]);
$this->title = $model->title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $model->description,
]);

 ?>
<section>
    <div class="row gallery-view">
            <div class="col-sm-8">
                <div class="main-news">
                    <a href="<?= \yii\helpers\Url::to(["gallery/index"])?>" class=" btn btn-danger">بازگشت</a>
                        <div class="news-title">
                            <h2><?= $model->title?> </h2>
                            <p><?= $model->description?></p>

                        </div>
                </div>
            </div>
        <div class="col-sm-4">
            <div class="property-slider">
                <div class="fotorama" data-nav="thumbs" data-autoplay="true" data-loop="true">
                    <?php
                    $images_file = json_decode($model->images_file);
                    foreach ($images_file as $item):
                        ?>
                        <a href="<?= Yii::getAlias('@uploads-url').'/gallery/'.$item;?>">
                            <img src="<?= Yii::getAlias('@uploads-url').'/gallery/'.$item;?>" alt="">
                        </a>
                    <?php endforeach; ?>
                </div>
            </div>

        </div>
    </div>

</section>

