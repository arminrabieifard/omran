<?php
/**
 * Created by PhpStorm.
 * User: armin
 * Date: 6/26/2018
 * Time: 8:13 PM
 */

$this->registerCssFile('@web/storage/css/general/owl.carousel.css');
$this->registerCssFile('@web/storage/css/general/owl.theme.default.min.css');
$this->registerJsFile('@web/storage/js/general/owl.carousel.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/storage/js/general/jquery.mousewheel.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->title = 'گالری تصاویر - عمران سازه شمال';
$this->registerMetaTag([
    'name' => 'description',
    'content' => '',
]);
?>
<section>
    <div class="gallery-main">
        <div class="col-sm-10 col-sm-offset-1 gallery">
            <section id="demos">
                <div class="row">
                    <div class="owl-carousel owl-theme owl-rtl">
                    <?php
                    foreach ($model2 as $value):
                    ?>
                        <div class="item">
                            <div class="item-img">
                                <img  class="img-circle" src="<?= Yii::getAlias('@uploads-url').'/gallery/'.$value->file_name?>" alt="<?= $value->title;?>">
                            </div>
                            <h3><?= $value->title;?></h3>
                        </div>
                        <?php endforeach; ?>

                    </div>
                </div>
            </section>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-1 label-item">
                <img  class="img-label" src="<?= Yii::getAlias('@storage-url').'/image/works.png'?>" alt="نمونه کارها">
            </div>
            <div class="col-sm-11 gallery-item">
                <div class="col-sm-11 works-item">
                    <?php
                    foreach ($model as $value):
                    ?>
                    <div class="col-sm-3">
                        <a href="<?= \yii\helpers\Url::to(['gallery/view',"id" => $value->id]);?>">
                        <div class="works-item-gallery">
                            <div class="works-image">
                                <img  class="" src="<?= Yii::getAlias('@uploads-url').'/gallery/'.$value->file_name?>" alt="<?= $value->title;?>">
                            </div>
                            <div class="work-title">
                                <h2><?= $value->title;?></h2>
                                <p><?= $value->description;?></p>
                            </div>
                        </div>
                        </a>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>


