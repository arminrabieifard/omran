<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'About';

$this->registerJsFile('https://maps.googleapis.com/maps/api/js?key=AIzaSyBSYMGwITgSxUlJMDRekzjQ-nUd5qVf2pk&callback=googleMap1',['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<?php  if (Yii::$app->session->hasFlash('alert')) {
    list($icon, $message) = Yii::$app->session->getFlash('alert');

    echo \yii\bootstrap\Alert::widget(['options' => [
        'class' => 'alert-' . $icon,
    ],
        'body' => $message,
    ]);
}?>
<div class="col-sm-12 about news-item">
        <div class="col-sm-1 label-item">
            <img  class="img-label" src="<?= Yii::getAlias('@storage-url').'/image/about.png'?>" alt="">

        </div>
        <div class="col-sm-11 news-item-description">


                    <div class="col-sm-8 about-desc">
                        <img  class="" src="<?= Yii::getAlias('@storage-url').'/image/about-pic.jpg'?>" alt="">
                        <div class="about-panell">

                            <h4><?= $about->title?></h4>
                            <p><?= $about->description?></p>
                            <div class="tellnumber">
                                <p><?= $tell->address?></p>
                                <p><?= $tell->tells?></p>
                                <p><?= $tell->email?></p>
                            </div>
                        </div>
                        <div class="map">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3175.1297258069562!2d49.58655989149443!3d37.268350779544654!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ff5623fe4f2718b%3A0xd8904b269e2cbfe2!2sRasht+Cafe!5e0!3m2!1sen!2sus!4v1530712773423" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="col-sm-4 tell-us">
                        <img  class="" src="<?= Yii::getAlias('@storage-url').'/image/call.jpg'?>" alt="">
                        <div class="call-us-panell form-group">

                            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                            <?= $form->field($model, 'full_name')->textInput(['placeholder' => 'نام کامل خود را وارد کنید...'], ['class' => 'form-control'], ['autofocus' => true])->label(false) ?>
                            <?= $form->field($model, 'email')->textInput(['placeholder' => 'آدرس ایمیل خودرا وارد کنید...'], ['class' => 'form-control'], ['autofocus' => true])->label(false) ?>
                            <?= $form->field($model, 'vCode')->widget(\yii\captcha\Captcha::className(), ['captchaAction' => '/site/captcha'])->label(false) ?>
                            <?= $form->field($model, 'message')->textarea(['placeholder' => 'متن خود را وارد کنید ....'], ['class' => 'form-control'], ['autofocus' => true])->label(false) ?>

                            <input type="submit" name="submit" value="ثبت پیام">
                            <?php ActiveForm::end();?>


                           </div>
                    </div>


            </div>
</div>





