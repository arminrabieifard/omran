<?php

/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'عمران سازه گیلان';
$this->registerMetaTag([
    'name' => 'description',
    'content' => '',
]);
?>

<section>
    <div class="col-sm-12 greenlight">
        <div class="col-sm-1 label-item">
            <img  class="img-label" src="<?= Yii::getAlias('@storage-url').'/image/service.png'?>" alt="Los Angeles">
        </div>
        <div class="col-sm-11 label-item">
            <div class="col-sm-8">
                <div class="tab-content">
                    <?php
                    $i = 0;
                    foreach ($serviceModel as $value):

                    ?>
                    <div id="menu<?=$value->id; ?>" class="tab-pane fade <?php if($i == 0){echo "active in";} ?>">
                        <h3><?=$value->title; ?></h3>
                        <p> <?=$value->description; ?>   </p>
                    </div>
                    <?php  $i++; endforeach;?>
                </div>
            </div>
            <div class="col-sm-4 left-menu-tab">
                <?php
                foreach ($serviceModel as $value):
                ?>
                   <div class=""><a data-toggle="tab" href="#menu<?= $value->id ?>"><?= $value->title ?>  </a></div>

                <?php endforeach;?>

            </div>



        </div>
</section>
<section>
    <div class="col-sm-12 works">
        <div class="col-sm-1 label-item">
            <a href="<?= Url::to(['gallery/'])?>"><img  class="img-label" src="<?= Yii::getAlias('@storage-url').'/image/works.png'?>" alt="Los Angeles"></a>
        </div>
        <div class="col-sm-11 works-item">
            <?php

            foreach ($galleryModel as $value):

            ?>
            <div class="col-sm-3">
                <a href="<?= Url::to(["gallery/view","id" => $value->id])?>">
                <div class="works-item-gallery">
                    <div class="works-image">
                        <img  class="" src="<?= Yii::getAlias('@uploads-url').'/gallery/'.$value->file_name;?>" alt="Los Angeles">
                    </div>
                    <div class="work-title">
                        <h2><?= $value->title;?></h2>
                        <p><?= $value->description;?></p>
                    </div>

                </div>
                </a>
            </div>
            <?php endforeach;?>
        </div>
    </div>
</section>

